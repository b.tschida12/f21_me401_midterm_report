#include <PID_v1.h>
#include <SoftPWMServo.h>

// Pin definitions
// DO NOT CHANGE THESE ONES
const int EncoderAPin = 2;
const int EncoderBPin = 20;
const int MotorDirectionPin = 4;
const int MotorPWMPin = 3;

// PID tuning gains
// float ku = FIND THIS; // ultimate gain
// float Tu = FIND THIS; // seconds
// double kp=0.8*ku,ki=0.0,kd=0.8*ku*Tu*0.125; // classic PD fomr Ziegler
// Nichols table

// PID Values for distance sensor rotor
double ku = 7, Tu = 0.16;  // 2.5 found, t=.1
// double kp = ku * 0.2, ki = 2 * (kp / tu), kd = kp * (tu / 3);
double kp = 0.8 * ku, ki = 0.0,
       kd = 0.8 * ku * Tu * 0.125;  // classic PD from Ziegler

// double kp=1.0,ki=0.0,kd=0.0; // stupid, simple, poor proportional controller

int LEFTBOOP = 0;
int RIGHTBOOP = 0;

// Global variables for quadrature decoder
static volatile char lastLeftA;
static volatile char lastLeftB;
static volatile bool errorLeft;
volatile long position = 0;
volatile int angle;

// Global variables for the timer interrupt handling
int pidSampleTime = 10;
long counterPID = 1;

// Global variables for the PID controller
double input = 0, output = 0, setpoint = 0;
PID myPID(&input, &output, &setpoint, kp, ki, kd, DIRECT);

// Forward declaration of functions to be used that are defined later than their
// first use
uint32_t TimerCallback(uint32_t currentTime);

void setupPIDandIR(void) {
  // Set up the quadrature inputs
  pinMode(EncoderAPin, INPUT);
  pinMode(EncoderBPin, INPUT);

  errorLeft = false;
  lastLeftA = digitalRead(EncoderAPin);
  lastLeftB = digitalRead(EncoderBPin);

  // Set up the motor outputs
  pinMode(MotorPWMPin, OUTPUT);
  pinMode(MotorDirectionPin, OUTPUT);

  digitalWrite(MotorPWMPin, 0);
  digitalWrite(MotorDirectionPin, 0);

  SoftPWMServoPWMWrite(MotorPWMPin, 0);

  // Setup the pid
  myPID.SetMode(AUTOMATIC);
  myPID.SetSampleTime(pidSampleTime);
  myPID.SetOutputLimits(-255, 255);

  // Initialize the timer interrupt that decodes the IR beacon signal
  attachCoreTimerService(TimerCallback);
}

uint32_t TimerCallback(uint32_t currentTime) {
  char newLeftA = digitalRead(EncoderAPin);
  char newLeftB = digitalRead(EncoderBPin);

  position += (newLeftA ^ lastLeftB) - (lastLeftA ^ newLeftB);

  if ((lastLeftA ^ newLeftA) & (lastLeftB ^ newLeftB)) {
    errorLeft = true;
  }

  lastLeftA = newLeftA;
  lastLeftB = newLeftB;

  if (counterPID % 100 * pidSampleTime == 0) {
    angle = map(position, 0, 2800, 0, 360);
    input = angle;

    myPID.Compute();

    if (output < 0) {
      digitalWrite(MotorDirectionPin, 1);
    } else {
      digitalWrite(MotorDirectionPin, 0);
    }
    SoftPWMServoPWMWrite(MotorPWMPin, abs(output));

    counterPID = 0;

    // TODO: READ SWITCHES
    int lVal = digitalRead(6);
    if (lVal == 1) {
      LEFTBOOP = 1;
    }
    int rVal = digitalRead(5);
    if (rVal == 1) {
      RIGHTBOOP = 1;
    }
  }
  counterPID++;

  return (currentTime + CORE_TICK_RATE / 100);
}
