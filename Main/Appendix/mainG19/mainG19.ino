// ME 401 Midterm project
// Group 19
// Jaida Woo, Jack Carbee, Reid Parsons, Ben Tschida

// PID Values
// double ku = 3.8, tu = 0.1;//2.5 found, t=.1
// double kp=ku*0.2, ki=2*(kp/tu), kd=kp*(tu/3);
//
// States:
//      Healing
//          - Use global data to drive to a zombie to heal them
//      Zombie
//          - find other robots and touch/attack based only on distance sensor
//      healthy
//          - avoid zombies using global data
//          - pick up ball if no zombies
//
// Transitions:
//      healthy -> healing : pick up ball
//      healing -> healthy : healed someone
//
//      healthy -> zombie : touched by zombie
//      zombie -> healthy : healed
//
#include <Servo.h>

#include "ME401_PID.h"
#include "ME401_Radio.h"
#include "MedianFilterLib.h"  // b/c MedianFilterLib.h is being stupid

#define MY_ROBOT_ID 19
#define MEDIAN_FILTER_DEPTH 7

enum stateMachineState {
  UNKNOWN = 0,
  HEALING = 1,
  HEALTHY = 2,
  ZOMBIE = 3,
  BACKLEFT = 4,
  BACKRIGHT = 5,
};

// rightservo speed % and ms to write
int r_speed = 0;
int rms;
// leftservo speed % and ms to write
int l_speed = 0;
int lms;

// Start Servos
Servo leftservo;
Servo rightservo;

// Global robot and ball positions
RobotPose robot;
BallPosition ball;

// Set default state
stateMachineState STATE = UNKNOWN;

// IR distance data pins
int IRSens1 = A0;
int IRSens2 = A1;

// Median Filter setup 
MedianFilter<float> medianFilter(MEDIAN_FILTER_DEPTH);

// robo array of current botties
RobotPose robots[NUM_ROBOTS];

// closest zombie coordinates
int zombx, zomby, zombdist;

// closest ball coordinates
int ballx, bally, balldist;

// Zombie state distance finding
// Defaults to impossibly large value for logic checks
int min_dist = 10000;
int min_ang = 10000;

int heal = 0;

int ledPin = 8;
bool ballCaptured = 0;

void setup() {
  Serial.begin(115200);
  ME401_Radio_initialize();
  setupPIDandIR();
  pinMode(ledPin, OUTPUT);
  // Setup servos
  leftservo.attach(33);
  rightservo.attach(32);

  // Hardware interrupts to spin around when hitting wall
  // attachInterrupt(lSwitch, backLeft, RISING);
  // attachInterrupt(rSwitch, backRight, RISING);
}

void loop() {
  // Check state and then execute each state code
  // Serial.println("LOOP");
  checkState();
  switch (STATE) {
    case (HEALTHY):
      healthyState();
      heal = 0;
      break;
    case (HEALING):
      healingState();
      break;
    case (ZOMBIE):
      zombieState();
      heal = 0;
      break;
    case (BACKLEFT):
      backRight();
      heal = 1;
      LEFTBOOP = 0;
      RIGHTBOOP = 0;
      spinWheel(0, 0);
      break;
    case (BACKRIGHT):
      backLeft();
      heal = 1;
      RIGHTBOOP = 0;
      LEFTBOOP = 0;
      spinWheel(0, 0);
      break;
    case (UNKNOWN):
      unknownState();
      break;
  }

  delay(10);
}

void checkState() {
  //  Serial.println("Checking State");
  updateRobotPoseAndBallPositions();  //! Main Update for Robot
  robot = getRobotPose(MY_ROBOT_ID);
  if (robot.valid == true) {
    if (LEFTBOOP == 1) {
      STATE = BACKLEFT;
    } else if (RIGHTBOOP == 1) {
      STATE = BACKRIGHT;
    } else if (robot.zombie) {
      STATE = ZOMBIE;
    } else if (robot.healing) {
      STATE = HEALING;
    } else if (!robot.healing && !robot.zombie) {
      STATE = HEALTHY;
    } else {
      STATE = UNKNOWN;
    }
  }
}

void zombieState() {
  // only use distance sensor to find targets
  Serial.println("In ZOMBIE State");

  zFindTarget();
  Serial.print("Target Dist: ");
  Serial.println(min_dist);
  Serial.print("Target Ang: ");
  Serial.println(min_ang);
  // float distTo = zAngleTarget();
  setpoint = 0;
  zDriveTo();
}

// Find nearest object, sweep from -60 to 60, finds min
void zFindTarget() {
  // Find distances at different angles of
  min_dist = 10000;
  //  delay(100);
  setpoint = -60;
  delay(50);
  while (setpoint <= 60) {
    int dist = distanceIR1();
    int ang = angle;  // Angle
    if (dist < min_dist) {
      min_dist = dist;
      min_ang = ang;
    }
    setpoint += 12;
    delay(100);
  }
  //  setpoint = -60;
}

void zDriveTo() {
  // Turn to an angle and drive forward
  // Stop after certain time to check targets again  //  float dist =
  // * turning towards target
  float kp = 1;

  int l_tspeed = 60;
  int r_tspeed = 60;

  if (abs(min_ang) > 5) {
    l_tspeed += -min_ang * kp;
    r_tspeed += min_ang * kp;
  }

  l_speed = l_tspeed + 20;
  r_speed = r_tspeed + 20;

  spinWheel(l_speed, r_speed);
}

// Uses camera data to find closest zombie/ball
void find_closest() {
  zombdist = 10000;
  for (int i = 0; i <= NUM_ROBOTS; i++) {
    RobotPose current = robotPoses[i];
    if (current.valid && current.zombie) {
      int dist = abs_dist(robot.x, robot.y, current.x, current.y);
      if (dist < zombdist) {
        zombdist = dist;
        zombx = current.x;
        zomby = current.y;
      }
    }
  }
  balldist = 10000;
  for (int i = 0; i <= numBalls; i++) {
    BallPosition curr = ballPositions[i];
    int dist = abs_dist(robot.x, robot.y, curr.x, curr.y);
    if (dist < balldist) {
      balldist = dist;
      ballx = curr.x;
      bally = curr.y;
    }
  }
}

void healingState() {
  Serial.println("In HEALING State");

  if (distanceIR2()) {
    ballCaptured = 1;
  }
  digitalWrite(ledPin, ballCaptured);

  if (heal < 1) {
    spinWheel(100, 100);
    delay(2000);
  }

  heal = 1;

  find_closest();
  if (zombdist < 10000) {
    drive_to(zombx, zomby);
  } else {
    drive_to(1200, 1200);
  }
  //      healing -> healthy : healed someone
  //      healing -> zombie : healed someone -> touched by zombie
}

void healthyState() {
  Serial.println("In HEALTHY state");
  digitalWrite(ledPin, 0);
  find_closest();
  //      healthy -> healing : pick up ball
  if (zombdist == 10000 || (balldist < zombdist && balldist != 10000)) {
    drive_to(ballx, bally);
  } else if (zombdist < 500) {
    int xdiff = robot.x - zombx;
    int ydiff = robot.y - zomby;
    drive_to(robot.x + xdiff, robot.y + ydiff);
  } else {
    spinWheel(0, 0);
  }
  //      healthy -> zombie : touched by zombie
}

void unknownState() {
  Serial.println("UNKNOWN STATE");
  //  distanceIR2();
}

void drive_to(int x, int y) {
  int dist = abs_dist(robot.x, robot.y, x, y);
  //  Serial.print("Absolute Distance: ");
  //  Serial.println(dist);

  float r_theta = (double)robot.theta * 0.001;
  //  Serial.print("ROBOT ANGLE: ");
  //  Serial.println(degrees(r_theta));
  // theta reported in rad*1000
  float cos_theta = cos(r_theta);
  float sin_theta = sin(r_theta);

  // 1. Align angle to point towards the point
  float dist_x =
      cos_theta * x + sin_theta * y - cos_theta * robot.x - sin_theta * robot.y;
  float dist_y = -sin_theta * x + cos_theta * y + sin_theta * robot.x -
                 cos_theta * robot.y;

  double des_angle = degrees(atan2(dist_y, dist_x));

  //  Serial.print("Y distance: ");
  //  Serial.println(dist_y);
  //  Serial.print("X distance: ");
  //  Serial.println(dist_x);
  //  Serial.print("Angle to Target: ");
  //  Serial.println(des_angle);
  //  Serial.print("X Target: ");
  //  Serial.println(x);
  //  Serial.print("Y Target: ");
  //  Serial.println(y);

  // * turning towards target
  float kp = 0.25;

  int fspeed = 50;
  float fgain = 0.1;
  if (dist < 500 && zombdist > 500) {
    fspeed = fgain * dist;  // dist * fgain;
  }

  int l_tspeed = 50;
  int r_tspeed = 50;

  if (abs(des_angle) > 10) {
    l_tspeed = -des_angle * kp;
    r_tspeed = des_angle * kp;
  }

  l_speed = l_tspeed + fspeed;
  r_speed = r_tspeed + fspeed;
  //
  //  Serial.print("Left Speed: ");
  //  Serial.println(l_speed);
  //  Serial.print("Right Speed: ");
  //  Serial.println(r_speed);
  spinWheel(l_speed, r_speed);
}

int abs_dist(int x1, int y1, int x2, int y2) {
  return sqrt(sq(x1 - x2) + sq(y1 - y2));
}

void spinWheel(int l_speed, int r_speed) {
  lms = map(l_speed, -100, 100, 1300, 1700);
  leftservo.writeMicroseconds(lms);
  rms = map(-r_speed, -100, 100, 1300, 1700);
  rightservo.writeMicroseconds(rms);
}

void backLeft() {
  backStraight();
  spinWheel(-50, 50);
  delay(650);
}

void backRight() {
  backStraight();
  spinWheel(50, -50);
  delay(650);
}

void backStraight() {
  spinWheel(-100, -100);
  delay(400);
}

// Distance sensor for finding robots
float ADC2Dist1(float median) { return 14788.05 * pow(median, -1.0685); }
float distanceIR1() {
  int median, irSensVal;
  for (int i = 0; i < MEDIAN_FILTER_DEPTH; i++) {
    irSensVal = analogRead(IRSens1);
    median = medianFilter.AddValue(irSensVal);
  }
  return ADC2Dist1(median);
}

// Distance sensor for determining ball captured
float ADC2Dist2(float median) {
  return 14788.05 * pow(median, -1.0685);  // ! NOT REAL VALUE
}

// Returns true when analog reading < 500
// Turns on LED to show ball is caputred
bool distanceIR2() {
  int median, irSensVal;
  for (int i = 0; i < MEDIAN_FILTER_DEPTH; i++) {
    irSensVal = analogRead(IRSens2);
    median = medianFilter.AddValue(irSensVal);
  }
  Serial.print("Analog Dist: ");
  Serial.println(irSensVal);
  Serial.print("Converted Dist: ");
  Serial.println(ADC2Dist2(median));

  return median < 500;
}
