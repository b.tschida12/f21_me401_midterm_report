%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% univreport.cls
%% Copyright 2021 B.Tschida
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is B.Tschida.
%
% This work is derived from article.cls, and provides 
% univreport.cls.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{univreport}



\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[]{article}



\def\@labtitle{No Title}
\newcommand{\labtitle}[1]{
    \def\@labtitle{#1}
}

\def\@shorttitle{No Title}
\newcommand{\shorttitle}[1]{
    \def\@shorttitle{#1}
}

\def\@labnumber{No lab number}
\newcommand{\labnumber}[1]{
    \def\@labnumber{#1}
}

\def\@labdate{No lab date}
\newcommand{\labdate}[1]{
    \def\@labdate{#1}
}

\def\@coursename{No course name}
\newcommand{\coursename}[1]{
    \def\@coursename{#1}
}

\def\@coursenum{No course number}
\newcommand{\coursenum}[1]{
    \def\@coursenum{#1}
}

\def\@courseinstructor{No course instructor}
\newcommand{\courseinstructor}[1]{
    \def\@courseinstructor{#1}
}

\def\@labinstructor{No lab instructor}
\newcommand{\labinstructor}[1]{
    \def\@labinstructor{#1}
}

\def\@coursesecnum{No section number}
\newcommand{\coursesecnum}[1]{
    \def\@coursesecnum{#1}
}

\def\@coursesecletter{No section letter}
\newcommand{\coursesecletter}[1]{
    \def\@coursesecletter{#1}
}

\def\@university{No University}
\newcommand{\university}[1]{
    \def\@university{#1}
}